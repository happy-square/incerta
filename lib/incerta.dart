import 'dart:async';

typedef PlainIncerta = Incerta<void, void>;
typedef GuardIncerta<T> = Incerta<T,Exception>;

/// Represents a async value.
/// Incerta is guaranteed to always be in one of the following states:
/// - hasData / IncertaData
/// - hasError / IncertaError
/// - isLoading / IncertaLoading
sealed class Incerta<V, E> {
  final bool hasData = false;
  final bool hasError = false;
  final bool isLoading = false;

  const Incerta._();

  static IncertaData<V, E> data<V, E>(V data) => IncertaData._(data);
  static IncertaError<V, E> error<V, E>(E error) => IncertaError._(error);
  static IncertaData<V?, E> emptyData<V, E>() => const IncertaData._(null);
  static IncertaError<V, E?> emptyError<V, E>() => const IncertaError._(null);
  static IncertaLoading<V, E> loading<V, E>() => const IncertaLoading._();

  IncertaData<V, E>? asData() => null;
  IncertaError<V, E>? asError() => null;
  IncertaLoading<V, E>? asLoading() => null;

  /// Reduces all possible Incerta states to a single type <T>.
  /// Transformation functions must be provided for each possible state.
  T reduce<T>({
    required T Function(V data) data,
    required T Function(E error) error,
    required T Function() loading,
  }) => throw UnimplementedError();

  /// Each possible Incerta state has a corresponding callback function
  /// which will be called if the state of this Incerta matches.
  void when({
    void Function(V data)? data,
    void Function(E error)? error,
    void Function()? loading,
  }) => throw UnimplementedError();

  void whenData(void Function(V data) onData) => when(data: onData);
  void whenError(void Function(E error) onError) => when(error: onError);
  void whenLoading(void Function() onLoading) => when(loading: onLoading);

  /// Maps this Incerta to a new one unsing the given tranformation functions
  Incerta<NewV, NewE> map<NewV, NewE>({
    required NewV Function(V value) data,
    required NewE Function(E error) error,
  }) => throw UnimplementedError();

  /// Maps this Incerta to a new one using the given data transformation function
  Incerta<NewV, E> mapData<NewV>(NewV Function(V data) transform)
    => map(data: transform, error: (error) => error);
  /// Maps this Incerta to a new one using the given error transformaiton function
  Incerta<V, NewT> mapError<NewT>(NewT Function(E value) transform)
    => map(data: (data) => data, error: transform);
}

/// Represents a Incerta state that has data (success state)
final class IncertaData<V, E> extends Incerta<V, E> {
  final V data;

  const IncertaData._(this.data) : super._();

  @override
  bool get hasData => true;

  @override
  IncertaData<V, E>? asData() => this;

  /// Reduces all possible Incerta states to a single type <T>.
  /// Transformation functions must be provided for each possible state.
  @override
  T reduce<T>({
    required T Function(V data) data,
    required T Function(E error) error,
    required T Function() loading
  }) => data(this.data);
  
  /// Each possible Incerta state has a corresponding callback function
  /// which will be called if the state of this Incerta matches.
  @override
  void when({
    void Function(V data)? data,
    void Function(E error)? error,
    void Function()? loading
  }) {
    data?.call(this.data);
  }

  /// Maps this Incerta to a new one unsing the given tranformation functions
  @override
  Incerta<NewV, NewE> map<NewV, NewE>({
    required NewV Function(V value) data,
    required NewE Function(E error) error
  }) => Incerta.data(data(this.data));
}

/// Represents a Incerta state that has an error (failure state)
final class IncertaError<V, E> extends Incerta<V, E> {
  final E error;

  const IncertaError._(this.error) : super._();

  @override
  bool get hasError => false;

  @override
  IncertaError<V, E>? asError() => this;

  /// Reduces all possible Incerta states to a single type <T>.
  /// Transformation functions must be provided for each possible state.
  @override
  T reduce<T>({
    required T Function(V data) data,
    required T Function(E error) error,
    required T Function() loading
  }) => error(this.error);

  /// Each possible Incerta state has a corresponding callback function
  /// which will be called if the state of this Incerta matches.
  @override
  void when({
    void Function(V data)? data,
    void Function(E error)? error,
    void Function()? loading
  }) => error?.call(this.error);

  /// Maps this Incerta to a new one unsing the given tranformation functions
  @override
  Incerta<NewV, NewE> map<NewV, NewE>({
    required NewV Function(V value) data, 
    required NewE Function(E error) error
  }) => Incerta.error(error(this.error));
}

/// Represents a Incerta state that is still loading (unfinished state)
final class IncertaLoading<V, E> extends Incerta<V, E> {
  const IncertaLoading._() : super._();

  @override
  bool get isLoading => true;

  @override
  IncertaLoading<V, E>? asLoading() => this;

  /// Reduces all possible Incerta states to a single type <T>.
  /// Transformation functions must be provided for each possible state.
  @override
  T reduce<T>({
    required T Function(V data) data,
    required T Function(E error) error,
    required T Function() loading
  }) => loading();

  /// Each possible Incerta state has a corresponding callback function
  /// which will be called if the state of this Incerta matches.
  @override
  void when({
    void Function(V data)? data,
    void Function(E error)? error,
    void Function()? loading
  }) => loading?.call();

  /// Maps this Incerta to a new one unsing the given tranformation functions
  @override
  Incerta<NewV, NewE> map<NewV, NewE>({
    required NewV Function(V value) data,
    required NewE Function(E error) error
  }) => Incerta.loading();
}

extension IncertaDelegation<T> on Future<T> {
  void delegate(void Function(Incerta<T, Exception> asyncResult) setter) async {
    setter(Incerta.loading());
    try {
      final result = await this;
      setter(Incerta.data(result));
    } on Exception catch (e) {
      setter(Incerta.error(e));
    }
  }

  Stream<Incerta<T, Exception>> delegateToStream(
    void Function(Incerta<T, Exception> asyncResult) setter
  ) async* {
    yield Incerta.loading();
    try {
      final result = await this;
      yield Incerta.data(result);
    } on Exception catch (e) {
      yield Incerta.error(e);
    }
  }
}
