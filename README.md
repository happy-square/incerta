Incerta is a standalone extension of riverpods AsyncValue that adds a generic error type.
Incerta values always represent one of these states:
- loading
- value (generically typed)
- error (generically typed)

## Getting Started
Add incerta to your pubspec.yaml dependencies: incerta: 0.2.1

## Notice
This package is currently under heavy development and subject to major changes until stable version 1.0.0!
You are better of using an exact version of Incerta in your pubspec.yaml.
